Установка приложения
====================

* В консоли выполнить следующие команды::

    git clone git@bitbucket.org:drnextgis/krasnogorsk.git
    cd krasnogorsk
    virtualenv --no-site-packages env
    source ./env/bin/activate
    pip install bottle psycopg2 sqlalchemy bottle-sqlalchemy pil waitress
    touch settings.py
   
* Файл `settings.py` должен содержать словарь настроек вида::
   
   settings={
       "sqlalchemy.url":'postgresql+psycopg2://username:password@localhost/dbname',
       "sqlalchemy.echo":True,
       "client_root": '/usr/home/tenzorr/projects/krasnogorsk/client'
    }

* Запуск приложения::

    python __init__.py

Деплоймент проекта в режиме Apache+mod_wsgi
===========================================

* Закомментируем в файле __init__.py последнюю строку::

    # run(app, host='0.0.0.0', port=8888, server='waitress')

* Устанавливаем mod_wsgi (предполагается, что Apache уже установлен и настроен)::

    sudo apt-get install libapache2-mod-wsgi

* Переходим в директорию sites-available::

    cd /etc/apache2/sites-available/

* Создаём файл с настройками хоста::

    touch krasnogorsk_http_api
  
  Пример содержимого файла::

    <VirtualHost *:80>
        ServerName krasnogorsk

        WSGIDaemonProcess krasnogorsk display-name=%{GROUP} threads=4 python-path=/home/rykovd/projects/krasnogorsk:/home/rykovd/projects/krasnogorsk/env/lib/python2.6/site-packages home=/home/rykovd/projects/krasnogorsk
        WSGIProcessGroup krasnogorsk
        WSGIScriptAlias / /home/rykovd/projects/krasnogorsk/deploy/handler.wsgi

        ErrorLog ${APACHE_LOG_DIR}/krasnogorsk_http_api_error.log

        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/krasnogorsk_http_api_access.log combined
    </VirtualHost>

* Активируем::

    sudo a2ensite krasnogorsk_http_api

* Создаём файл /home/rykovd/projects/krasnogorsk/deploy/handler.wsgi::

    from __init__ import app
    application = app

* Перезапускаем Apache::

    sudo service apache2 restart

