# -*- coding: utf-8 -*-

import Image
import urllib2
from bottle import Bottle
from bottle import HTTPError
from bottle import run
from bottle import response
from bottle import request
from bottle import template
from bottle import static_file
from models import sqlalchemy_plugin
from models import FeaturePhoto
from settings import settings
from sqlalchemy import func
from sqlalchemy import select
from sqlalchemy.sql import null
from sqlalchemy.orm import aliased
from sqlalchemy.orm.exc import NoResultFound
from StringIO import StringIO
from hashlib import md5

app = Bottle()
app.install(sqlalchemy_plugin)


class RestController(object):

    def __init__(self, model, app, collection_url, item_url):
        self._model = model
        self._table = model.__tablename__

        views = (
            ('query', collection_url, 'GET'),
            ('read', item_url, 'GET'),
            ('create', collection_url, 'POST'),
            ('update', item_url, 'PUT'),
            ('delete', item_url, 'DELETE'),
        )

        for route_name, url, method in views:
            app.route(url, method, getattr(self, 'view_%s' % route_name), self.route(route_name))

    def route(self, name):
        return 'api_%s_%s' % (self._table, name)

    def as_dict(self, row):
        return row.obj.as_dict()

    def query(self, db, filter_by):
        return db.query(aliased(self._model, name='obj'), null()) \
            .filter_by(**filter_by)

    def view_query(self, db, **kwargs):
        pass

    def view_read(self, db, **kwargs):
        pass

    def view_delete(self, db, **kwargs):
        pass

    def view_create(self, db, **kwargs):
        pass

    def view_update(self, db, **kwargs):
        pass


class ImageController(RestController):

    def url(self, **kwargs):
        return app.get_url(self.route('read'), **kwargs)

    def role_verify(self, db, role, passwd):
        passwd_digest = md5('%s%s' % (passwd, role)).hexdigest()
        return db.connection().execute("SELECT passwd FROM pg_shadow WHERE usename=%s", role).scalar() == '%s%s' % ('md5', passwd_digest)

    def privilege_verify(self, db, role, table, privilege):
        return db.query(func.has_table_privilege(role, table, privilege)).scalar()

    def credentials(self, request):
        return request.headers.get('X-Role'), request.headers.get('X-Password')

    def view_query(self, db, **kwargs):
        query = db.query(self._model).filter_by(**kwargs)
        if request.query.outputFormat != 'html':
            return dict(images=[dict(id=row.id, url=self.url(id=row.id)) for row in query.all()])
        else:
            return template('images_html', dict(feature_ids=[row.id for row in query.all()]))

    def view_read(self, db, **kwargs):
        try:
            image_type = request.query.type
            query = db.query(self._model).filter_by(**kwargs).one()
            if query.original is None:
                raise NoResultFound
            if image_type != 'preview':
                output = query.original
            else:
                if query.preview is not None:
                    output = query.preview
                else:
                    temp_input_file = StringIO(query.original)
                    temp_output_file = StringIO()
                    temp_image = Image.open(temp_input_file)
                    temp_image.thumbnail((128, 128), Image.ANTIALIAS)
                    temp_image.save(temp_output_file, "PNG")
                    output = temp_output_file.getvalue()
                    db.query(self._model).filter_by(id=query.id).update({'preview': output})
            response.content_type = 'image'
            return output
        except NoResultFound:
            return HTTPError(404)

    def view_create(self, db, **kwargs):
        role, passwd = self.credentials(request)
        if self.role_verify(db, role, passwd) and self.privilege_verify(db, role, self._table, 'insert'):
            obj = self._model(**kwargs)
            data = request.files.data
            obj.original = data.file.read()
            db.add(obj)
            db.flush()

            pks = dict()
            for pk in self._model.__table__.primary_key:
                pks[pk.name] = getattr(obj, pk.name)
            row = db.query(self._model).filter_by(**pks).one()

            return dict(url=self.url(id=row.id))
        else:
            return HTTPError(403)

    def view_delete(self, db, **kwargs):
        role, passwd = self.credentials(request)
        if self.role_verify(db, role, passwd) and self.privilege_verify(db, role, self._table, 'delete'):
            try:
                row = self.query(db, kwargs).one()
                db.delete(row.obj)
                response.status = 204
            except NoResultFound:
                return HTTPError(404)
        else:
            return HTTPError(403)

ImageController(FeaturePhoto, app, '/api/<typename>/<feature_id:int>/images/', '/api/images/<id:int>')


# For testing purpose only!
@app.route('/upload/<typename>/<feature_id>')
def upload_form(typename, feature_id):
    return template('upload', dict(typename=typename, feature_id=feature_id))


@app.route('/client/<path:path>')
def client_app(path):
    return static_file(path, root=settings.get('client_root'))


@app.route('/proxy')
def openlayers_proxy():
    url = request.query.get('url')
    url_obj = urllib2.urlopen(url)
    url_meta = url_obj.info()
    response.set_header('Content-Type', url_meta['Content-Type'])
    return url_obj.read()


run(app, host='0.0.0.0', port=8888, server='waitress')
