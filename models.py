# -*- coding: utf-8 -*-

from bottle.ext import sqlalchemy
from sqlalchemy import engine_from_config
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import Unicode
from sqlalchemy import LargeBinary
from sqlalchemy.ext.declarative import declarative_base
from settings import settings

Base = declarative_base()
engine = engine_from_config(settings, 'sqlalchemy.')

sqlalchemy_plugin = sqlalchemy.Plugin(
    engine,
    Base.metadata,
    keyword='db',
    commit=True,
    use_kwargs=False
)


class FeaturePhoto(Base):
    __tablename__ = 'photo_table'
    id = Column(Integer, primary_key=True)
    feature_id = Column(Integer, nullable=False)
    typename = Column(Unicode(80), nullable=False)
    original = Column(LargeBinary)
    preview = Column(LargeBinary)
    __table_args__ = {'schema': 'public'}

    def as_dict(self, **addon):
        return dict(id=self.id, **addon)
